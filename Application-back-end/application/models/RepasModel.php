<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class RepasModel extends CI_Model{
	
	public function GetRepas(){
		$query = $this->db->get('repas');
        return $query->result();
	}

	public function insertRepas($nom_repas){
		$requete="INSERT into repas(nom_repas) values(%s)";
		echo $nom_repas;
        $requete=sprintf($requete,$this->db->escape($nom_repas));         
        $query=$this->db->query($requete);
	}
}
