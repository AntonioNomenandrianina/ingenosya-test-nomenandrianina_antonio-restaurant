<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class IngredientModel extends CI_Model{
	
	public function GetRepas(){
		$query = $this->db->get('ingredients');
        return $query->result();
	}

	public function insert_ingredient($data){
		$this->db->set($data);
		return $this->db->insert('ingredients', $data);
	}

	public function getwhere($id){
		$query = $this->db->query("SELECT * FROM ingredients where id_repas="+$id);
		return $query->result();
	}


}
