<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Repas extends REST_Controller{
	
	public function __construct() {
		parent::__construct();
		$this->load->database();
  	}

	/**
	 * Get All Data from this method.
	 *
	 * @return Response
	 */
	public function index_get($id = 0)
	{
		if(!empty($id)){
			$data = $this->db->get_where("repas", ['id_repas' => $id])->row_array();
		}else{
			$data = $this->db->get("repas")->result();
		}
		$this->response($data, REST_Controller::HTTP_OK);
	}

		/**
	 * Get All Data from this method.
	 *
	 * @return Response
	 */
	public function index_post()
	{
		$input = json_decode(file_get_contents('php://input'),true);
		$data = array(
			'nom_repas' => $this->input->post('nom_repas')
		);
		$this->db->set($data);
		$result = $this->db->insert('repas',$input);
		if($result > 0)
		{
			$this->response([
				'status' => 200,
				'message' => 'NEW REPAS CREATED'
			], REST_Controller::HTTP_OK); 
		}
		else
		{
			$this->response([
				'status' => 404,
				'message' => 'FAILED TO CREATE NEW INGREDIENT'
			], REST_Controller::HTTP_BAD_REQUEST);
		}
	}


}
