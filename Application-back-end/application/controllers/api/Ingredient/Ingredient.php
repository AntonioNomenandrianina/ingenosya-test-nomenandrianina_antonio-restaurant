<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: ACCEPT, CONTENT-TYPE, X-CSRF-TOKEN");
header("Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS, DELETE");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With"); 

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Ingredient extends REST_Controller{
	
	public function __construct() {
		parent::__construct();
		$this->load->database();
  	}


	/**
	 * Get All Data from this method.
	 *
	 * @return Response
	 */
	public function index_get($id = 0)
	{
		if(!empty($id)){
			$data = $this->db->get_where("ingredients", ['id_repas' => $id])->result_array();
		}else{
			$data = $this->db->get("ingredients")->result();
		}
		$this->response($data, REST_Controller::HTTP_OK);
	}

		/**
	 * Get All Data from this method.
	 *
	 * @return Response
	 */
	public function index_post()
	{
		$input = json_decode(file_get_contents('php://input'),true);
		$data = array(
			'id_repas' =>  $this->input->post('id_repas'),
			'nom_ingredient' => $this->input->post('nom_ingredient'),
			'quantite' => $this->input->post('quantite'),
			'unite' => $this->input->post('unite'),
		);
		$this->db->set($data);
		$result = $this->db->insert('ingredients',$input);
		if($result > 0)
		{
			$this->response([
				'status' => 200,
				'message' => 'NEW INGREDIENT CREATED'
			], REST_Controller::HTTP_OK); 
		}
		else
		{
			$this->response([
				'status' => 404,
				'message' => 'FAILED TO CREATE NEW INGREDIENT'
			], REST_Controller::HTTP_BAD_REQUEST);
		}
	}

	


}
