CREATE DATABASE restaurent;


CREATE TABLE repas(
	id_repas int PRIMARY KEY NOT NULL AUTO_INCREMENT,
	nom_repas VARCHAR(50)
);

INSERT INTO repas (nom_repas) VALUES('Humberger');
INSERT INTO repas (nom_repas) VALUES('Yaourt');
INSERT INTO repas (nom_repas) VALUES('Panini');
INSERT INTO repas (nom_repas) VALUES('Pizza');
INSERT INTO repas (nom_repas) VALUES('Firtte');


CREATE TABLE ingredients(
	id_ingredient int PRIMARY KEY NOT NULL AUTO_INCREMENT,
	id_repas int,
	nom_ingredient VARCHAR(50),
	quantite float,
	unite VARCHAR(50),
	FOREIGN KEY (id_repas) REFERENCES repas(id_repas)
);
