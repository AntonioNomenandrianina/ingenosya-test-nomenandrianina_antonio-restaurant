import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './accueil/index/index.component';
import { SaisieIngredientsComponent } from './repas/saisie-ingredients/saisie-ingredients.component';
import { AjoutRepasComponent } from './repas/ajout-repas/ajout-repas.component';
import { IngredientComponent } from './repas/ingredient/ingredient.component';


const routes: Routes = [
  { path: '', redirectTo: 'index', pathMatch: 'full' },
  { path: 'index', component: IndexComponent },
  { path: 'repas/saisie-ingredients', component: SaisieIngredientsComponent },
  { path: 'repas/ajout-repas', component: AjoutRepasComponent },
  { path: 'ingredient/:id_repas', component: IngredientComponent },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
