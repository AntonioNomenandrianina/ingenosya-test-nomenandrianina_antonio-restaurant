import { HttpResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { Repas } from 'src/app/repas';
import { RepasService } from 'src/app/Services/repas.service';



@Component({
  selector: 'app-saisie-ingredients',
  templateUrl: './saisie-ingredients.component.html',
  styleUrls: ['./saisie-ingredients.component.css']
})
export class SaisieIngredientsComponent implements OnInit {

  public loading = false;
  message: any;
  food: Repas[] = [];
  ingredient = new FormGroup({
    id_repas:  new FormControl(null, [ Validators.required]),
    nom_ingredient:  new FormControl(null, [ Validators.required]),
    quantite: new FormControl(null, [Validators.required]),
    unite: new FormControl(null, [Validators.required])
  });

  get id_repas(){return this.ingredient.get('id_repas'); }
  get nom_ingredient(){return this.ingredient.get('nom_ingredient'); }
  get quantite(){return this.ingredient.get('quantite'); }
  get unite(){return this.ingredient.get('unite'); }

  displayedColumns: string[] = ['position', 'name', 'action'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private repasService: RepasService) { }

  ngOnInit(): void {
    this.GetFood();
  }



  GetFood(){
    this.loading = true;
    this.repasService.getRepas().subscribe((data) =>  {
      this.food = data;
      this.dataSource = new MatTableDataSource(this.food);
      this.dataSource.paginator = this.paginator;
      this.loading = false;
    });
  }

  save_ingredient(){
    this.loading = true;
    this.loading = true;
    const onSuccess = (response: any) => {
        if (response.status === 200){
            this.loading = false;
            this.ingredient.reset();
            this.message = response.message;
            // this.router.navigateByUrl('/home');
        }else{
          this.message = response.message;
          this.loading = false;
          this.ingredient.reset();
        }
    };
    const onError = (response: any) => {
      if (response.status === 400){
          this.message = response.message;
          this.loading = true;
          this.ingredient.reset();
      }
    };
    const data = {
      id_repas: this.ingredient.value.id_repas,
      nom_ingredient: this.ingredient.value.nom_ingredient,
      quantite: this.ingredient.value.quantite,
      unite: this.ingredient.value.unite
    };
    this.repasService.insert_ingredient(data).subscribe(onSuccess, onError);
  }



}
