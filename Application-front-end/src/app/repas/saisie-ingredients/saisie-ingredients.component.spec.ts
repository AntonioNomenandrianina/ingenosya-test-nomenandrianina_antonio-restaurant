import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaisieIngredientsComponent } from './saisie-ingredients.component';

describe('SaisieIngredientsComponent', () => {
  let component: SaisieIngredientsComponent;
  let fixture: ComponentFixture<SaisieIngredientsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaisieIngredientsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaisieIngredientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
