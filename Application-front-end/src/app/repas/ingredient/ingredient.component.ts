import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RepasService } from 'src/app/Services/repas.service';

@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.css']
})
export class IngredientComponent implements OnInit {

  repas: any;
  ingredient: any;
  public loading: false;
  id = this.route.snapshot.paramMap.get('id_repas');
  constructor(private route: ActivatedRoute, private repasService: RepasService) { }

  ngOnInit(): void {
    console.log(this.id);
    this.GetRepas();
    this.GetIngredient();
  }

  GetRepas(){
    this.repasService.GetRepasById(this.id).subscribe((data) =>  {
      this.repas = data;
      console.log(this.repas);
    });
  }

  GetIngredient(){
    this.repasService.GetIngredientById(this.id).subscribe((data) =>  {
      this.ingredient = data;
      console.log(this.ingredient);
    });
  }

}
