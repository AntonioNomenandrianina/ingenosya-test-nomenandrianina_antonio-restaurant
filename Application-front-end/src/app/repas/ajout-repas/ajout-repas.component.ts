import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { Repas } from 'src/app/repas';
import { RepasService } from 'src/app/Services/repas.service';

@Component({
  selector: 'app-ajout-repas',
  templateUrl: './ajout-repas.component.html',
  styleUrls: ['./ajout-repas.component.css']
})
export class AjoutRepasComponent implements OnInit {

  public loading = false;
  message: any;
  food: Repas[] = [];
  repas = new FormGroup({
    nom_repas:  new FormControl(null, [ Validators.required])
  });

  get nom_repas(){return this.repas.get('nom_repas'); }


  displayedColumns: string[] = ['position', 'name', 'action'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private repasService: RepasService) { }

  ngOnInit(): void {
    this.GetFood();
  }

  GetFood(){
    this.loading = true;
    this.repasService.getRepas().subscribe((data) =>  {
      this.food = data;
      this.loading = false;
      this.dataSource = new MatTableDataSource(this.food);
      this.dataSource.paginator = this.paginator;

    });
  }

  save_repas(){
    this.loading = true;
    const onSuccess = (response: any) => {
        if (response.status === 200){
            this.loading = false;
            this.repas.reset();
            this.message = response.message;
            // this.router.navigateByUrl('/home');
        }else{
          this.message = response.message;
          this.loading = false;
          this.repas.reset();
        }
    };
    const onError = (response: any) => {
      if (response.status === 400){
          this.message = response.message;
          this.loading = true;
          this.repas.reset();
      }
    };
    const data = {
      nom_repas: this.repas.value.nom_repas,
    };
    this.repasService.insert_repas(data).subscribe(onSuccess, onError);
  }


}
