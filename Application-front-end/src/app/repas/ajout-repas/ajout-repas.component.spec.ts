import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutRepasComponent } from './ajout-repas.component';

describe('AjoutRepasComponent', () => {
  let component: AjoutRepasComponent;
  let fixture: ComponentFixture<AjoutRepasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjoutRepasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutRepasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
