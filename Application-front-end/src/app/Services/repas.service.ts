import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { baseUrl } from 'src/environments/environment.prod';
import { Repas } from '../repas';

const urlRepas = baseUrl + 'api/Repas/Repas';
const urlIngredient = baseUrl + 'api/Ingredient/Ingredient';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
};


@Injectable({
  providedIn: 'root'
})
export class RepasService {

  constructor(private http: HttpClient) { }

  getRepas(): Observable<Repas[]> {
    return this.http.get<Repas[]>(baseUrl + '/api/Repas/Repas');
  }
  insert_repas(data){
    return this.http.post(baseUrl + 'api/Repas/Repas/', data);
  }
  insert_ingredient(data){
    return this.http.post(baseUrl + 'api/ingredient/ingredient/', data);
  }
  GetRepasById(idrepas) {
    return this.http.get(`${urlRepas}/${idrepas}`);
  }
  GetIngredientById(idrepas){
    return this.http.get(`${urlIngredient}/${idrepas}`);
  }
}
